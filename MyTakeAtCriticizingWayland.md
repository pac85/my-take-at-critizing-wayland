# My take at criticizing wayland (and pipewire)

I've been criticizing wayland for a long time without really managing to engage in a real technical discussion.
Finally I've decided to collect all of my critiques in a text file so that I don't have to start from scratch 
each time I try.

## What wayland is trying to do


Wayland is a protocol and a collection of libraries that aims at making it possible to implement compositors that should be 
able to replace Xorg. Now Xorg is a really old piece of software and it certainly has some limitations, however one thing
people tend to gloss over is the fact that Xorg has also been evolving for all that time. Now evolution usually doesn't lead
to the best possible solution, however it usually manages to adapt whatever is evolving to it's enviroment. The job of 
someone wanting to replace something that's really old with an entirely new thing is to find which problem the old thing
solved, which problems it didn't ever manage to solve, and to find an alegant solution to all of those. This is really hard
especially bacause which problem a piece of software solves is subjective. Ask the author or user of a WM such as i3, 
XMonad or somthing similar which problem X solved for them and you might get as an answer that Xorg allows them to not focus
on details about compositing, input handling or something like that and instead focus and things that are more bound to the
task of WM. Ask a GNOME developer and he would probably never mention such problem since they envision a more integrated 
experience and therfore need to worry about those details anyway. Althouhg I respect the people behind Wayland I don't think
they are doing a great job at finding all of the problems that X solved. I will talk about some of those problems that
X solves for me but that X doesn't but first I want to give my opinion on the way wayland supposedly solves some problems.


## Security

Security is one of the of the things wayland is advertized as being good at. However I think it gets it foundamentally wrong.
Let's start by trying to define what security means in the context of a display server protocol: I'd expect a secure protocol 
to somehow be able to authenticate it's clients so that the server can allow/disallow different actions based on who he is 
talking with. In the end that's why X is not secure, with no authentication mechanism there's no way to impose policies and
therefore everyone can do everything.
Wayland has no such authentication mechanism either though. Instead wayland simply doesn't allow clients to do certain things 
such as capturing the screen or listening for keypress events while not focused. So you can think of wayland securiry as 
taking your precious money and jewelry, sealing it inside a metal box, burrying it very deep and banging your head against the
wall untill you forget where you burried it. Surely it's very unlickely that someone will be able to steal it but you also 
won't be able to find it when you need it. So what happens with wayland when you need to capture your screen or to bind some
global hotkeys? None of those issue are dealt with by wayland. Screen Capture is usually handled with pipewire about which 
I will talk in the next section but global keybingins are even worse. In fact only rarely you'll be able to get away
with what your WM offers. Usually that would be a way to run some program in response to some key presses, but that cannot help
you if the program you want to use doesn't have some command line tool that uses IPC to comunicate that event to the main 
process. For example if you want to use push to talk in telegram, discord or some other similar program you are out of luck.
People behind wayland seem to be against the idea of a protocol that globally exposes keypresses, which is understandable. A
proposal was made for a safe protocol which would allow to associate keys with events that selected apps would receive but it
is unlickley to be accepted as it is clumsy and pretty much impossinle to abstract in toolkits.



## Pipewire

Pipewire supposedly solves many security issues, some unrelated to wayland such as those related to audio, some related
to wayland such as screen capture. Let's talk a bit about the latter. For now we won't worry about how clients talk to 
pipwire, we'll just assume it somehow happens securely. What we will worry about is how pipewire gets the image from the 
compositor. The compositor is the only thing with enough knowledge about what's going on on the screen (the contents itself
is known by the driver and can be accessed if a user has sufficient permissions but that's not a practical way of doing screen
capture and it doesn't allow features such as capturing a single window). So the way it works is that some process needs to 
register some dbus objects to allow pipewire to access those buffers. In sway (or other wlroots based WMs) that is not done by 
the main WM process, which brings a question: how does that process get access to those buffers from the WM? It happens through
a wlroots specific protocol called wlr-screencopy (there is also wlr-expor-dmabuf which is faster for captuiring videos).
But wait, we just said that wayland has no authentication mechanism, how are those protocols secured? Well...they aren't, any
program that has access to the compositor also gets access to that api, neet uh? Now it's easy to blame wlroots's author for
doing what seems like a stupid move but honestly I see no way of securely implementing such a protocol. One approach would be 
to not use the wayland IPC mechanism and instead make the compositor directly use something like dbus. This is what gnome and
plasma are actually doing(and I know some programs use it directly too so I'm not sure on how secure it is). But then again 
wayland isn't solving a problem here, it's simply not caring about it leading people to use other systems to solve it. 
Now let's talk about the other side of things. How does pipewire securely talk to a client?
Much like anything else talking with pipewire is done through a socket and once again pipewire has no way of telling who the 
hell is making requests, instead something colled a "session manager" should identify clients and accordingly set pipewire's 
objets permissions. Current implementations are dummies and always set full permissions to all objects. The way apps are 
prevented from accessing those within flatpak is by using "portals". Portals are nothing more than dbus api to request 
resources. The idea is that the sanbox would implement such APIs, use whatever mechanism it wants to let the user decide which
resources should be accessible from inside the sanbox and then return some handle to those resources. Pipewire allows to hide
objets from a connection and therefore allows for the fine grained security that is needed. However that mechanism that I just described translates to
a significant amount of security critical code and no generic implementation of it exists as far as I know. This means that 
outside of flatpak (and snap apparently) those mechanism are currently not available. Honestly I wouldn't trust flatpak 
security as [they don't seem to have taken it seriously](https://flatkill.org/) . Now althought this hole thing could be 
implemented in other sanboxing frameworks it is still a rather complicated way of handling it and can't easily be done by invoking
a bunch of cli tools as it heavly relies on being able to control a dbus connection. Also the fact that they worry so much 
about implementing it in flatpak instead of giving some generic framework tells you a lot about how much they care about 
use cases different from their's.



## Low latency for video games

Let's talk about something entirely different. Let's first start by giving an abstract model of a video game. We will think
of a video game as some black box that spits out frames. The job of a compositor is to (aside from providing events and other 
things) somehow get those frame on the user's screen. It seems simple but there's a problem: games usually involve several 
complex calculations in order to draw those frames so the time it takes to process one of them can very while the display always
runs at a fixed rate (unless you own some of those fancy freesync or gsync enabled monitors). How do we make it work? There are
several ways but the main ones are:
* FIFO: in this mode a queue keeps track of when the client makes a request to present a new frame. Usually this causes the client to wait until a vertical blank happens
* MAILBOX (or triple buffering): the MW will wait a blanking interval before presenting an image but the client keeps running indepenently. If the client finished rendering a frame before the previous frame finishes rendering then that frame is replaced by the latest one.
* IMMEDIATE: Nor the app nor the compositor wait for a vertical blank. The frame ends up on the screen as soon as possible.

The first two won't result into tearing, the last one might. The first one causes the client to run to a framerate that is a 
submultiple of the screen refresh rate. The lower the frame rate the lower the rate at which the event queue is pumped so 
the longer the atency.

In the second mode the client will run as fast as it can, and therfore it will poll the input as fast as it can but in order for
the resulting frame to be displayed a vblank event has to happen so it still introduces a bit of input latency.

The last one dowsn't wait for vblank at any point. This means that input is polled as soon as possinble and the result is displayed
as soon as possible meaning that you get as low of a input latency as possible. However this results in visible screen tearing.
Through vulkan the client can explicitly request one of the three modes, it makes sense to use IMMEDIATE for a competitive
first person shooter were latency is everything, while for other types of game the first two might be more appropriate. Most games
actually allow the player to chose between the first and the last, with some giving the three options directly to the player.
Now wayland adopts the *every frame is pergect* filosofy, this means that if IMMEDIATE present mode is requested no tearing will be 
visible. This effectively means that IMMEDIATE behaves pretty much like MAILBOX. This does not violate the vulkan spec as some devices 
simply can't have tearing so the spec does not require for the implementation to actually do it, however this is not accepable for many 
users. It is ironic because among the few open source games there are several fast peaced first person shooters for which IMMEDIATE
present mode would be most appropriate. A [feature request](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/65) 
was made in order to allow wsi to ask the compositor for tearing updates but 5 months after that nothing as changed(I will talk about
how slow wayland is at accepting protocols later). But an even bigger problem is that OpenGL (and specifically the present extention used
with wayland) doesn't really allow to specify those 3 options. It only allows to speicfy a "swap interval" integer. Historycally a value
of 0 was intepreted as IMMEDIATE while 1 was intepretaed as FIFO. More modern systems would only allow tearing updates when the app doesn't
go through a compositor (such as when the app is in full screen). In wayland 0 means MAILBOX and 1 means FIFO and this cannot change as it could
cause apps that assume this behaviour to teare. So
you won't be able to play OpenArena or sauerbraten with proper input latency, ever (unless you [modify the compositor](https://gitlab.com/pac85/wlroots-no-vsync) ).



## Fragmentation

One foundamental difference between Wayland and Xorg is that Wayland is just a protocol while Xorg is a piece of software. Not only that
Wayland actually has a tiny core and relies on "protocols" (basically extenions) to allow even basic things. This is beacause Wayland doesn't
just want to work on desktops, it wants to be as general as possible.
Some protocols, such as xdg-shell, are available in all WMs and desktop enviroments, but some are specific to the implementation such as the aforementioned 
wlr-screencopy protocol for taking screenshots under wlroots based WMs. So what does this mean for a developer? Suppose you wanted to make a 
status bar, or a dock, surely you would want it to work with as many WMs as possible. X provides all you need in it's protocol and you'll talk
to the same server regardless of the WM. With wayland though it's a different story. Wayland itself doesn't know what a bar or a dock are, nor
does it let you access the information you need about clients since that wouldn't be secure. So once again you are out of luck. Wlroots does provie
a protocol that would work for all wlroots based WMs, but that's not all WMs. BTW I think sway's author deserves a lot for actually caring about
other projects as opposed to other developers that don't really seem to care. If wlroots itself was proposed as a substitute to Xorg instead 
of just a protocol I'd be much happier.


## Slow development

Wayland was introduced around 2010. We had to wait until 2015 to get [a protocol](https://github.com/wayland-project/wayland-protocols/blob/master/unstable/pointer-constraints/pointer-constraints-unstable-v1.xml)
that would allow games and other apps to lock the pointer to their window, until 2016 for another [protocol](https://lists.freedesktop.org/archives/wayland-devel/2016-August/030864.html) 
that would allow VM software and others to grab keyboard and mouse, as I'm writing this there are discutions on how to manage focus requests.

Imagine being a developer and being told that the protocol that you need has only been discussed about for 5 months and it will take years to actually
implement and stabilize it. 


## But it is a feature
Say you are a developer of a screen recording application and you somehow managed to chose among the many ways of recording the screen under 
wayland WMs. Now you want the user to be able to start and stop the recording with a key combo. You'd be told that you can't do that under 
waylnd but that you should be hapy about it 'cause that's a (security) feature! Ain't that great? No, not being able to make legit apps beacuse
wayland can't authenticate clients and thefore disallows every exploitable action isn't great.
Same goes if you want to make some kind of automation software.


## Drivers

Input devices in wayland are handled by libinput.
Libinput was initially part of weston, it is a library providing all you need to interact with all sorts of devices. There is nothing wrong with
libinput onto iself however I'd like to compare it with how Xorg manages input devices. Xorg itelf doesn't know about evdev or about whatever 
mechanism a device uses to comunicate with the userspace. Instead Xorg allows for drivers to be dynamically loaded. Those are essentialy shared
libraries. Xorg's protocol is also as flexible as to allow tools such as xsetwacom to comunicate with such drivers in order to configure whatever
parameter you might need. X also manages many things by itself so for example you can change your keyboard layout to us by typing setxkbmap us, or
confine the range of an input device to a certain monitor by typing xinput map-to-output device-id monitor, this works regardless of the WM and 
device in question. With wayland there is no such thing. For one as far as wayland is concerned you could get your input for wherever you want.
But assuming the WM us using libinput, first libinput needs to know the specifics of that device, and second configuring some aspects of it will 
be specific to the wm(surely libinput doesn't know about your monitor). If the device is not supported by libinput you can't just load a driver.

Output devices are handled similarly by Xorg. Xorg loads a driver that can use whatever kernel API it wants to draw whatever it is asked to draw.
Don't have a GPU? No problem, xf86-video-fbturbo will implement that functionality in software and rely on fbdev to display stuff on your screen
which pretty much always works. With wayland the WM does everything by itelf and ususally they require DRM/KMS/GBM. 


## Conclusion

In the end my Point is that Wayland, as of now, doesn't do a great job at replacing X which, by the way, deserves lots of respect for working as 
well as it does after 27 or so years.  Some things might get fixed, some surely won't, some are being worked around in weird ways. Honestly I hate 
the fact that Xorg is being abandoned so long before there is a valid alternative.


